simple文件夹里面是个解决方案，建议用Visual Studio 2012打开.
-angularjs_basic.html演示了最基本的用法
-angularjs_shoppingcart是个简单的购物车
-angular_route.html演示了如何动态加载模板，制作SPA（Single Page Application，单页面应用）应用

AngularJSIn60MinutesIsh.pdf
-很好的入门教程

AngularJS中文版.pdf
-这也是很好的入门教程，适合对E文不友好的读，不过貌似这本书比较老，实例代码中错误较多

最牛逼的demo应该是：Learning AngularJS by Example – The Customer Manager Application，把这个搞明白了，应该能很轻松地开发一般需求的CRUD（增加Create、查询Retrieve、更新Update、删除Delete）的应用啦。
网址为：http://weblogs.asp.net/dwahlin/learning-angularjs-by-example-the-customer-manager-application