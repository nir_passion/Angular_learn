/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//function MyController($scope, $timeout) {
//    var updateClock = function () {
//        $scope.clock = new Date();
//        $timeout(function () {
//            updateClock();
//        }, 1000);
//    };
//    updateClock();
//}

function MyController($scope) {
    $scope.clock = {
        now: new Date()
    }
    var updateClock = function () {
        $scope.clock.now = new Date();
    };
    setInterval(function () {
        $scope.$apply(updateClock);
    }, 1000);
    updateClock();
}

