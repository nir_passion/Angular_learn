/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('myApp', [])
        .directive('ensureUnique', ['$http', function ($http) {
                return {
                    require: 'ngModel',
                    link: function (scope, ele, attrs, c) {
                        scope.$watch(attrs.ngModel, function () {
                            $http({
                                method: 'POST',
                                url: '/api/check/' + attrs.ensureUnique,
                                data: {'field': attrs.ensureUnique}
                            }).success(function (data, status, headers, cfg) {
                                c.$setValidity('unique', data.isUnique);
                            }).error(function (data, status, headers, cfg) {
                                c.$setValidity('unique', false);
                            });
                        });
                    }
                };
            }]);

