'use strict';
/* global angular */

angular.module('navApp', [])
        .directive('sideBox', function () {
            return {
                restrict: 'A',
                scope: {
                    title: '@'
                },
                transclude: true,
                template: '<div class="sidebox"><div class="content"><h2 class="header">' +
                        '{{ title }}</h2><span class="content" ng-transclude></span></div></div>'
            };
        });

angular.module('docsIsoFnBindExample', [])
        .controller('Controller', ['$scope', '$timeout', function ($scope, $timeout) {
                $scope.name = 'Tobias';
                $scope.hideDialog = function () {
                    $scope.dialogIsHidden = true;
                    $timeout(function () {
                        $scope.dialogIsHidden = false;
                    }, 2000);
                };
            }])
        .directive('myDialog', function () {
            return {
                restrict: 'E',
                transclude: true,
                scope: {
                    'close': '&onClose'
                },
                templateUrl: 'my-dialog-close.html'
            };
        });

