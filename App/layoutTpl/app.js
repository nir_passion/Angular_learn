'use strict';
/* global angular */

angular.module('myApp', [])
        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider
                        .when('/', {
                            templateUrl: 'views/home.html',
                            controller: 'HomeCtrl'
                        })
                        .when('/', {
                            templateUrl: 'views/login.html',
                            controller: 'LoginCtrl'
                        })
                        .when('/', {
                            templateUrl: 'views/dashboard.html',
                            controller: 'DashCtrl'
                        })
                        .otherwise({
                            redirectTo: '/'
                        });
            }]);


