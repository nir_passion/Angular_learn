
'use strict';
/* global angular */
angular.module('myApp', [])
        .run(function ($rootScope) {
            $rootScope.rootProperty = 'root Scope';
        })
        .controller('ParentCtrl', function ($scope) {
            $scope.parentProperty = 'parent scope';
        })
        .controller('ChildCtrl', function ($scope) {
            $scope.childProperty = 'child scope';
            $scope.fullSentenceFromChild = 'Same $scope: We can access: ' +
                    $scope.rootProperty + ' and ' +
                    $scope.parentProperty + ' and ' +
                    $scope.childProperty;
        });

angular.module('myApp_03', [])
        .run(function ($rootScope, $timeout) {
            $rootScope.isDisabled = true;
            $timeout(function () {
                $rootScope.isDisabled = false;
            }, 5000);
        })
        .controller('mainCtrl', function ($scope) {

        })
        .directive('myDirective', function () {
            return {
                restrict: 'A',
                scope: {},
                template: 'Inside myDirective, isolate scope: {{ myProperty }}'
            };
        })
        .directive('myInheritScopeDirective', function () {
            return {
                restrict: 'A',
                template: 'Inside myDirective, isolate scope: {{ myProperty }}',
                scope: true
            };
        });
