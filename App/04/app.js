'use strict';
/* global angular,controller */

angular.module('timeExampleModule', [])
        .factory('time', function ($timeout) {
            var time = {};
            (function tick() {
                var date = new Date();
                time.now = date.toLocaleString();
                $timeout(tick, 1000);
            })();
            return time;
        })
        .controller('ClockCtrl', function ($scope, time) {
            $scope.time = time;
        })
        .controller('ParentCtrl', function ($scope) {
            $scope.Value = 'ParentValue';
            $scope.ParentAction = function () {
                $scope.Value = 'ParentValue Changed';
            };
        })
        .controller('ChildCtrl', function ($scope) {//值的复制
            $scope.ChildAction = function () {
                $scope.Value = 'ChildValue Changed';
            };
        })
        .controller('FirstCtrl', function ($scope) {
            $scope.someModel = {
                value: '默认值'
            };
            $scope.FirstAction = function () {
                $scope.someModel.value = 'FirstCtrl Changed';
            };
        })
        .controller('SecondCtrl', function ($scope) {//对象的引用
            $scope.SecondAction = function () {
                $scope.someModel.value = 'SecondCtrl Changed';
            };
        })
        .controller('myCtrl', function ($scope) {
            $scope.eq = {};
            $scope.change = function () {
                $scope.eq.output = parseInt($scope.eq.x) + 2;
            };
        });

angular.module('direcApp', [])
        .controller('dirCtrl', function ($scope) {

        })
        .controller('ddCtrl', function ($scope) {

        })
        .controller('mainCtrl', function ($scope) {

        })
        .directive('myDirective', function () {
            return{
                restrict: 'A',
                scope: {}, //false 继承但不隔离 || true 继承并隔离 || {} 隔离且不继承
                priority: 100,
                template: '<div>内部:{{myPro}}<input ng-model="myPro"/></div>'
            };
        });

angular.module('formApp', [])
        .controller('startCtrl', function ($scope) {

            $scope.computeNeeded = function () {
                $scope.needed = $scope.startingEstimate * 10;
            };

            $scope.requestFunding = function () {
                window.alert("Sorry, please get more customers first.");
            };

        })
        .controller('restTab', function ($scope) {

            $scope.directory = [
                {name: 'The Handsome Heifer', cuisine: 'BBQ'},
                {name: 'Green\' s Green  Greens ', cuisine: ' Salads '},
                {name: 'House of Fine  Fish ', cuisine: ' Seafood '}
            ];
            
            $scope.selectRestaurant = function (row) {
                console.log(row);
                $scope.selectedRow = row;
            };
            
        });
