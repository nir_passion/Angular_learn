'use strict';
/*global angular*/

angular.module('myApp.services', [])
        .factory('githubService', function ($http) {
            var githubUrl = 'https://api.github.com';
            var githubUsername;
            var runUserRequest = function (path) {
                return $http({
                    method: 'JSONP',
                    url: githubUrl + '/users/' + githubUsername + '/' + path + '?callback=JSON_CALLBACK'
                });
            };
            return {
                events: function () {
                    return runUserRequest('events');
                },
                setUsername:function(username){
                    githubUsername = username;
                }
            };
        });

angular.module('myApp', ['myApp.services'])
        .controller('ServiceController', function ($scope, $timeout, githubService) {
            $scope.setUsername = githubService.setUsername;
            var timeout;
            $scope.$watch('username', function (newUsername) {
                if (newUsername) {
                    if (timeout)
                        $timeout.cancel(timeout);
                    timeout = $timeout(function () {
                        githubService.events(newUsername)
                                .success(function (data, status) {
                                    $scope.events = data.data;
                                });
                    },350);
                }
            });
            
        });


