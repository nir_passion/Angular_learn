/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//angular.module('emailParser', [])
//        .config(['$interpolateProvider'], function ($interpolateProvider) {
//            $interpolateProvider.startSymbol('__');
//            $interpolateProvider.endSymbol('__');
//        })
//        .factory('EmailParser', ['$interpolate', function ($interpolate) {
//                return {
//                    parse: function (text, context) {
//                        var template = $interpolate(text);
//                        return template(context);
//                    }
//                };
//            }]);
//
//angular.module('myApp', ['emailParser'])
//        .controller('MyController', ['$scope', 'EmailParser',
//            function ($scope, EmailParser) {
//                $scope.to = 'ari@fullstack.io';
//                $scope.emailBody = 'Hello__to__';
//                //设置监听
//                $scope.$watch('emailBody', function (body) {
//                    if (body) {
//                        $scope.previewText = EmailParser.parse(body, {
//                            to: $scope.to
//                        });
//                    }
//                });
//            }]);
angular.module('myApp', ['emailParser'])
      .controller('MyController',
        ['$scope', 'EmailParser',
          function($scope, EmailParser) {
            $scope.to = 'ari@fullstack.io';
            $scope.emailBody = 'Hello __to__';
            // Set up a watch
            $scope.$watch('emailBody', function(body) {
              if (body) {
                $scope.previewText =
                  EmailParser.parse(body, {
                    to: $scope.to
                  });
              }
            });
    }]);

    angular.module('emailParser', [])
      .config(['$interpolateProvider',
        function($interpolateProvider) {
          $interpolateProvider.startSymbol('__');
          $interpolateProvider.endSymbol('__');
    }])
    .factory('EmailParser', ['$interpolate',
      function($interpolate) {
        // a service to handle parsing
        return {
          parse: function(text, context) {
            var template = $interpolate(text);
            return template(context);
          }
        };
    }]);

